<?php

use App\Shop\Countries\Country;
use Illuminate\Database\Seeder;

class MyCountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert(array (
            0 =>
                array (
                    'id' => '100',
                    'iso' => 'ID',
                    'name' => 'INDONESIA',
                    'iso3' => 'IDN',
                    'numcode' => '360',
                    'phonecode' => '62',
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                )
        ));
    }
}
