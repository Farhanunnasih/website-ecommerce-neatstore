<?php

use Illuminate\Database\Seeder;

class MyCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert(array (
            0 =>
                array (
                    'name' => 'Bandung',
                    'province_id' => '7',
                ),
            1 =>
                array (
                    'name' => 'Jakarta',
                    'province_id' => '5',
                ),
            2 =>
                array (
                    'name' => 'Surabaya',
                    'province_id' => '9',
                ),
            3 =>
                array (
                    'name' => 'Medan',
                    'province_id' => '34',
                ),
        ));
    }
}
