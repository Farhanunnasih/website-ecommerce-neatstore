<?php

use Illuminate\Database\Seeder;

class MyProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'country_id' => 100,
                    'name' => 'Aceh',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            1 =>
                array (
                    'id' => 2,
                    'country_id' => 100,
                    'name' => 'Banten',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            2 =>
                array (
                    'id' => 3,
                    'country_id' => 100,
                    'name' => 'Bengkulu',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            3 =>
                array (
                    'id' => 4,
                    'country_id' => 100,
                    'name' => 'Gorontalo',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            4 =>
                array (
                    'id' => 5,
                    'country_id' => 100,
                    'name' => 'Jakarta',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            5 =>
                array (
                    'id' => 6,
                    'country_id' => 100,
                    'name' => 'Jambi',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            6 =>
                array (
                    'id' => 7,
                    'country_id' => 100,
                    'name' => 'Jawa Barat',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            7 =>
                array (
                    'id' => 8,
                    'country_id' => 100,
                    'name' => 'Jawa Tengah',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            8 =>
                array (
                    'id' => 9,
                    'country_id' => 100,
                    'name' => 'Jawa Timur',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            9 =>
                array (
                    'id' => 10,
                    'country_id' => 100,
                    'name' => 'Banten',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            10 =>
                array (
                    'id' => 11,
                    'country_id' => 100,
                    'name' => 'Kalimantan Barat',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            11 =>
                array (
                    'id' => 12,
                    'country_id' => 100,
                    'name' => 'Kalimantan Selatan',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            12 =>
                array (
                    'id' => 13,
                    'country_id' => 100,
                    'name' => 'Kalimantan Tengah',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            13 =>
                array (
                    'id' => 14,
                    'country_id' => 100,
                    'name' => 'Kalimantan Timur',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            14 =>
                array (
                    'id' => 15,
                    'country_id' => 100,
                    'name' => 'Kalimantan Utara',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            15 =>
                array (
                    'id' => 16,
                    'country_id' => 100,
                    'name' => 'Kepulauan Bangka Belitung',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            16 =>
                array (
                    'id' => 17,
                    'country_id' => 100,
                    'name' => 'Kepulauan Riau',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            17 =>
                array (
                    'id' => 18,
                    'country_id' => 100,
                    'name' => 'Lampung',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            18 =>
                array (
                    'id' => 19,
                    'country_id' => 100,
                    'name' => 'Maluku',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            19 =>
                array (
                    'id' => 20,
                    'country_id' => 100,
                    'name' => 'Maluku Utara',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            20 =>
                array (
                    'id' => 21,
                    'country_id' => 100,
                    'name' => 'Bali',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            21 =>
                array (
                    'id' => 22,
                    'country_id' => 100,
                    'name' => 'Nusa Tenggara Barat',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            22 =>
                array (
                    'id' => 23,
                    'country_id' => 100,
                    'name' => 'Nusa Tenggara Timur',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            23 =>
                array (
                    'id' => 24,
                    'country_id' => 100,
                    'name' => 'Papua',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            24 =>
                array (
                    'id' => 25,
                    'country_id' => 100,
                    'name' => 'Papua Barat',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            25 =>
                array (
                    'id' => 26,
                    'country_id' => 100,
                    'name' => 'Riau',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            26 =>
                array (
                    'id' => 27,
                    'country_id' => 100,
                    'name' => 'Sulawesi Barat',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            27 =>
                array (
                    'id' => 28,
                    'country_id' => 100,
                    'name' => 'Sulawesi Selatan',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            28 =>
                array (
                    'id' => 29,
                    'country_id' => 100,
                    'name' => 'Sulawesi Tengah',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            29 =>
                array (
                    'id' => 30,
                    'country_id' => 100,
                    'name' => 'Sulawesi Tenggara',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            30 =>
                array (
                    'id' => 31,
                    'country_id' => 100,
                    'name' => 'Sulawesi Utara',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            31 =>
                array (
                    'id' => 32,
                    'country_id' => 100,
                    'name' => 'Sumatra Barat',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            32 =>
                array (
                    'id' => 33,
                    'country_id' => 100,
                    'name' => 'Sumatra Selatan',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            33 =>
                array (
                    'id' => 34,
                    'country_id' => 100,
                    'name' => 'Sumatra Utara',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            34 =>
                array (
                    'id' => 35,
                    'country_id' => 100,
                    'name' => 'Yogyakarta',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),

        ));
    }
}
