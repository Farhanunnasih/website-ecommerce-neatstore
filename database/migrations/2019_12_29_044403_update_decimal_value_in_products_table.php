<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDecimalValueInProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('price')->change();
            $table->integer('weight')->change();
            $table->integer('length')->change();
            $table->integer('width')->change();
            $table->integer('height')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('price', 8, 2)->change();
            $table->decimal('weight', 8, 2)->change();
            $table->decimal('length', 8, 2)->change();
            $table->decimal('width', 8, 2)->change();
            $table->decimal('height', 8, 2)->change();
        });
    }
}
