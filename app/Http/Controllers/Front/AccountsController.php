<?php

namespace App\Http\Controllers\Front;

use App\Shop\Cities\City;
use App\Shop\Cities\Repositories\Interfaces\CityRepositoryInterface;
use App\Shop\Countries\Country;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Orders\Order;
use App\Shop\Orders\Transformers\OrderTransformable;
use App\Shop\Provinces\Province;
use App\Shop\Provinces\Repositories\Interfaces\ProvinceRepositoryInterface;

class AccountsController extends Controller
{
    use OrderTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    private $cityRepo;
    private $provinceRepo;

    /**
     * AccountsController constructor.
     *
     * @param CourierRepositoryInterface $courierRepository
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        CourierRepositoryInterface $courierRepository,
        CustomerRepositoryInterface $customerRepository,
        ProvinceRepositoryInterface $provinceRepository,
        CityRepositoryInterface $cityRepository
    ) {
        $this->customerRepo = $customerRepository;
        $this->courierRepo = $courierRepository;
        $this->provinceRepo = $provinceRepository;
        $this->cityRepo = $cityRepository;
    }

    public function index()
    {
        $customer = $this->customerRepo->findCustomerById(auth()->user()->id);

        $customerRepo = new CustomerRepository($customer);
//        $orders = $customerRepo->findOrders(['*'], 'created_at');
//
//        $orders->transform(function (Order $order) {
//            return $this->transformOrder($order);
//        });

        $addresses = $customerRepo->findAddresses();

        $country = Country::find($addresses->pluck('country_id'))->first();
        $province = Province::find($addresses->pluck('province_id'))->first();
        $city = $addresses->pluck('city')->first();
        $zip = $addresses->pluck('zip')->first();
        $street = $addresses->pluck('address')->first();

        $address = $country->name.", ".$province->name.", ".$city.", ".$zip." ".$street;

        return view('altfront.profile', [
            'user' => $customer,
            //'orders' => $this->customerRepo->paginateArrayResults($orders->toArray(), 15),
            'addresses' => $addresses,
            'address' => $address,
        ]);
    }
}
