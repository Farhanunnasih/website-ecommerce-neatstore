<?php

namespace App\Http\Controllers\Front;

use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Product;
use App\Shop\Products\Transformations\ProductTransformable;
use Illuminate\Http\Request;

class HomeController
{
    use ProductTransformable;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;
    private $productRepo;

    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        ProductRepositoryInterface $productRepository
    ) {
        $this->categoryRepo = $categoryRepository;
        $this->productRepo = $productRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cat1 = null;
        $cat2 = null;
        /*$cat1 = $this->categoryRepo->findCategoryById(2);
        $cat2 = $this->categoryRepo->findCategoryById(3);*/

        return view('front.index', compact('cat1', 'cat2'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function altIndex(Request $request)
    {
        $list = $this->productRepo->listProducts('id');

        $products = $list->map(function (Product $item) {
            return $this->transformProduct($item);
        })->paginate(10);

        if ($request->ajax()) {
            return view('altfront.shared.products', [ 'products' => $products ]);
        }

        //$this->productRepo->paginateArrayResults($products, 15)
        return view('altfront.index', [ 'products' => $products ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function shop()
    {
        $list = $this->productRepo->listProducts('id');

        $products = $list->map(function (Product $item) {
            return $this->transformProduct($item);
        })->all();

        return view('altfront.shop', [
            'products' => $this->productRepo->paginateArrayResults($products, 15)
        ]);
    }

    public function blog()
    {

        return view('altfront.blog');
    }

    public function about()
    {

        return view('altfront.about');
    }
}
