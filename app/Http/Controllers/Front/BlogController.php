<?php

namespace App\Http\Controllers\Front;

use App\Shop\News\Repository\NewsRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * @var NewsRepositoryInterface
     */
    private $newsRepo;

    /**
     * @var int
     */
    private $paginateCount = 5;

    /**
     * BlogController constructor.
     * @param NewsRepositoryInterface $newsRepository
     */
    public function __construct(NewsRepositoryInterface $newsRepository)
    {
        $this->newsRepo = $newsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $news = $this->newsRepo->listNews(['*'], 'id', 'asc')->paginate($this->paginateCount);

        if ($request->ajax()) {
            return view('altfront.shared.blog-post', ['news' => $news]);
        }

        return view('altfront.blog', ['news' => $news]);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $news = $this->newsRepo->findNewsById($id);
        return view('altfront.shared.blog-post-detail', ['post' => $news]);
    }


}
