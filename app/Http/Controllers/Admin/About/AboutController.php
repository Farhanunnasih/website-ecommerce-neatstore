<?php

namespace App\Http\Controllers\Admin\About;

use App\Http\Requests\Shop\About\Request\CreateAboutRequest;
use App\Http\Requests\Shop\About\Request\UpdateAboutRequest;
use App\Shop\About\Repositories\AboutRepository;
use App\Shop\About\Repositories\AboutRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    /**
     * @var AboutRepositoryInterface
     */
    private $aboutRepo;

    /**
     * AboutController constructor.
     * @param AboutRepositoryInterface $aboutRepository
     */
    public function __construct(AboutRepositoryInterface $aboutRepository)
    {
        $this->aboutRepo = $aboutRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->aboutRepo->listAbouts(['*'], 'id', 'asc')->all();

        return view('admin.about.list', ['abouts' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about.create');
    }


    public function store(CreateAboutRequest $request)
    {
        $this->aboutRepo->createAbout($request->except('_token', '_method'));

        return redirect()->route('admin.abouts.index')->with('message', 'Section Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $about = $this->aboutRepo->findOneOrFail($id);

        return view('admin.about.show', ['about' => $about]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = $this->aboutRepo->findAboutById($id);

        return view('admin.about.edit', ['about' => $about]);
    }

    /**
     * @param UpdateAboutRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\UpdateAboutException
     */
    public function update(UpdateAboutRequest $request, $id)
    {
        $about = $this->aboutRepo->findAboutById($id);

        $update = new AboutRepository($about);
        $update->updateAbout($request->except('_token', '_method'));

        request()->session()->flash('message', 'Update Succesful');
        return redirect()->route('admin.abouts.index', $id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $about = $this->aboutRepo->findAboutById($id);
        $about->delete();

        request()->session()->flash('message', 'Delete successful');
        return redirect()->route('admin.abouts.index');
    }
}
