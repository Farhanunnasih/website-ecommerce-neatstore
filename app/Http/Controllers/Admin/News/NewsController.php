<?php

namespace App\Http\Controllers\Admin\News;

use App\Exceptions\CreateNewsException;
use App\Shop\News\Repository\NewsRepository;
use App\Shop\News\Repository\NewsRepositoryInterface;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class NewsController extends Controller
{
    use UploadableTrait;

    private $newsRepo;

    public function __construct(NewsRepositoryInterface $newsRepository)
    {
        $this->newsRepo = $newsRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->newsRepo->listNews(['*'], 'id', 'asc')->all();

        /*if (request()->has('q') && request()->input('q') != '') {
            $list = $this->productRepo->searchProduct(request()->input('q'));
        }*/

        return view('admin.news.list', ['news' => $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token', '_method');
        $data['slug'] = str_slug($request->input('title'));

        if ($request->hasFile('cover') && $request->file('cover') instanceof UploadedFile) {
            $data['cover'] = $this->newsRepo->saveCoverImage($request->file('cover'));
        }

        $news = $this->newsRepo->createNews($data);

        return redirect()->route('admin.news.index')->with('message', 'Article successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = $this->newsRepo->findNewsById($id);
        return view('admin.news.show', ['post' => $news]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
