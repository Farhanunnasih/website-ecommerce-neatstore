<?php


namespace App\Shop\About\Repositories;


use App\Exceptions\CreateAboutException;
use App\Exceptions\UpdateAboutException;
use App\Shop\About\About;
use App\Shop\Categories\Exceptions\CategoryInvalidArgumentException;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AboutRepository extends BaseRepository implements AboutRepositoryInterface
{
    use UploadableTrait;

    public function __construct(About $about)
    {
        parent::__construct($about);
        $this->model = $about;
    }

    /**
     * @param array $params
     * @return About
     */
    public function createAbout(array $params): About
    {
        try {

            $collection = collect($params);

            $title = $params['title'];

            if (isset($params['cover']) && ($params['cover'] instanceof UploadedFile)) {
                $cover = $this->uploadOne($params['cover'], 'images/about');
            }

            $merge = $collection->merge(compact('title', 'cover'));

            $about = new About($merge->all());

            $about->save();
            return $about;
        } catch (QueryException $e) {
            throw new CategoryInvalidArgumentException($e->getMessage());
        }
    }

    public function findAboutById(int $id): About
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e);
        }
    }

    /**
     * @param array $params
     * @return About
     * @throws UpdateAboutException
     */
    public function updateAbout(array $params): About
    {
        try {
            $about = $this->findAboutById($this->model->id);
            $collection = collect($params)->except('_token');
            $title = $params['title'];

            if (isset($params['cover']) && ($params['cover'] instanceof UploadedFile)) {
                $cover = $this->uploadOne($params['cover'], 'images/about');
            }

            if (isset($cover)) {
                $merge = $collection->merge(compact('title', 'cover'));
            } else {
                $merge = $collection->merge(compact('title'));
            }

            $about->update($merge->all());
            return $about;
        } catch (QueryException $e) {
            throw new UpdateAboutException($e);
        }
    }

    public function deleteAbout(): bool
    {
        return $this->delete();
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return Collection
     */
    public function listAbouts($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc'): Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    public function saveAbout(About $about)
    {

        // TODO: Implement saveAbout() method.
    }
}