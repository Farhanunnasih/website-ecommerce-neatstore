<?php


namespace App\Shop\About\Repositories;


use App\Shop\About\About;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface AboutRepositoryInterface extends BaseRepositoryInterface
{
    public function createAbout(array $data) : About;

    public function findAboutById(int $id) : About;

    public function updateAbout(array $data) : About;

    public function deleteAbout() : bool;

    public function listAbouts($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    public function saveAbout(About $about);



}