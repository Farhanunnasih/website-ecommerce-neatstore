<?php

namespace App\Shop\About;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
        'title',
        'content',
        'cover'
        ];
}
