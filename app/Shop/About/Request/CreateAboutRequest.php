<?php

namespace App\Http\Requests\Shop\About\Request;

use App\Shop\Base\BaseFormRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateAboutRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'content' => ['required']
        ];
    }
}
