<?php

namespace App\Shop\News;

use App\NewsImage;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title',
        'article',
        'cover',
        'slug'
    ];

}
