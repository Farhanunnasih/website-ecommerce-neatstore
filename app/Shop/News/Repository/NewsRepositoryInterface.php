<?php


namespace App\Shop\News\Repository;


use App\Shop\News\News;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface NewsRepositoryInterface extends BaseRepositoryInterface
{
    public function createNews(array $data) : News;

    public function findNewsById(int $id) : News;

    public function updateNews(array $data) : News;

    public function deleteNews(News $news) : bool;

    public function listNews($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

    public function saveCoverImage(UploadedFile $file) : string;

    public function saveNews(News $news);

}