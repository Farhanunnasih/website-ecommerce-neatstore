<?php


namespace App\Shop\News\Repository;


use App\Exceptions\CreateNewsException;
use App\Exceptions\DeleteNewsException;
use App\Exceptions\UpdateNewsException;
use App\Shop\News\News;
use App\Shop\Tools\UploadableTrait;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Intervention\Image\Facades\Image;
use Jsdecena\Baserepo\BaseRepository;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class NewsRepository extends BaseRepository implements NewsRepositoryInterface
{
    use UploadableTrait;

    public function __construct(News $news)
    {
        parent::__construct($news);
        $this->model = $news;
    }


    public function createNews(array $data): News
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CreateNewsException($e);
        }
    }

    public function findNewsById(int $id): News
    {
        return $this->findOneOrFail($id);
    }

    public function updateNews(array $data): News
    {
        $filtered = collect($data)->all();

        try {
            $this->model->where('id', $this->model->id)->update($filtered);
        } catch (QueryException $e) {
            throw new UpdateNewsException($e);
        }
    }

    public function deleteNews(News $news): bool
    {
        try {
            return $news->delete();
        } catch (\Exception $e) {
            throw new DeleteNewsException($e);
        }
    }

    public function listNews($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc'): Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }



    public function saveNews(News $news)
    {
        // TODO: Implement saveNews() method.
    }

    public function saveCoverImage(UploadedFile $file): string
    {
        $img = Image::make($file->getFileInfo());
        $imgName = md5($file->getFilename());
        $mime = explode('/', $img->mime);

        $fileName = "blogs/".$imgName.".".$mime[1];
        $img
            ->fit(300)
            ->save("storage/".$fileName);

        return $fileName;
    }
}