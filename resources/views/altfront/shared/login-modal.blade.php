<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            {{--<div class="limiter">

            </div>--}}
            {{--<div class="container-login100">

            </div>--}}
            <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
                <form action="{{ route('home') }}"  method="POST" class="login100-form validate-form flex-sb flex-w">
                    {{ csrf_field() }}
					<span class="login100-form-title p-b-32">
						Account Login
					</span>
                    @include('layouts.errors-and-messages')

                    <span class="txt1 p-b-11">
						Username
					</span>
                    <div class="wrap-input100 validate-input m-b-36" data-validate = "Username is required">
                        <input class="input100" type="text" name="email" >
                        <span class="focus-input100"></span>
                    </div>

                    <span class="txt1 p-b-11">
						Password
					</span>
                    <div class="wrap-input100 validate-input m-b-12" data-validate = "Password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
                        <input class="input100" type="password" name="password" >
                        <span class="focus-input100"></span>
                    </div>

                    <div class="flex-sb-m w-full p-b-48">
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox100" for="ckb1">
                                Remember me
                            </label>
                        </div>

                        <div>
                            <a href="#" class="txt3">
                                Forgot Password?
                            </a>
                        </div>
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn" type="submit">
                            Login
                        </button>
                    </div>

                </form>
            </div>

            <div id="dropDownSelect1"></div>

        </div>
    </div>
</div>

