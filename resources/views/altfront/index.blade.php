@extends('layouts.altfront.app')

@section('title')
    Home
@endsection

@section('header')

    @include('layouts.altfront.includes.header-three')

@endsection

@section('content')
    @include('altfront.shared.login-modal')

    @include('altfront.shared.slider')

    @include('altfront.shared.banner')

    <!-- Product -->
    <section class="bg0 p-t-23 p-b-130">
        <div class="container">
            <div class="p-b-10">
                <h3 class="ltext-103 cl5">
                    Product Overview
                </h3>
            </div>

            <div id="prod-container">
                @include('altfront.shared.products')
            </div>

            <!-- Pagination -->
            {!! $products->links('altfront.pagination.pagination')  !!}
        </div>
    </section>

@endsection

@section('js')
    <script src="{{ asset('js/jquery-3.4.1.js') }}"></script>
    <script type="text/javascript">
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page === Number.NaN || page <= 0) {
                    return false;
                } else {
                    getData(page);
                }
            }
        });

        $(document).ready(function () {
            $("a.how-pagination1").on('click', function (e) {
                e.preventDefault();
                $("a.how-pagination1").removeClass('active-pagination1');
                $(this).addClass('active-pagination1');

                var myurl = $(this).attr('href');
                var page = myurl.split('page=')[1];

                getData(page);
                $('#search-bar').scrollIntoView();
            });
        });

        function getData(page) {
            $.ajax(
                {
                    url: '?page=' + page,
                    type: "GET",
                    dataType: "html",
                }
            ).done(function (data) {
                $("#prod-container").empty().html(data);
                location.hash = page;
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from the server');
            })
        }
    </script>
@endsection