@extends('layouts.altfront.app')

@section('title')
    Shop
@endsection

@section('header')

    @include('layouts.altfront.includes.header-one')

@endsection

@section('content')

    @include('altfront.shared.products')

@endsection