@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        <div class="box">
            <form action="{{ route('admin.news.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title">Title <span class="text-danger">*</span></label>
                        <input type="text" name="title" id="title" placeholder="Title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="cover">Cover <span class="text-danger">*</span></label>
                        <input type="file" name="cover" id="cover" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="title">Articles <span class="text-danger">*</span></label>
                        <textarea name="article" id="article" rows="5" placeholder="Article Content" class="form-control ckeditor"></textarea>
                    </div>
                </div>
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.news.index') }}" class="btn btn-default">Back</a>
                    <button class="btn btn-primary" type="submit">Create</button>
                </div>
            </div>
            </form>

        </div>


    </section>
    <!-- /.content -->
@endsection