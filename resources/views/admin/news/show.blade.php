@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        @if($post)
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $post->title }}</h3>
                </div>

                <div class="box-body">
                    <table>
                        <thead>
                            <tr>
                                <td>Cover</td>
                                <td>Title</td>
                                <td>Article</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $post->title }}</td>
                                <td>
                                    @if(isset($post->cover))
                                        <img src="{{ asset("storage/".$post->cover) }}" alt="" class="img-responsive">
                                    @endif
                                </td>
                                <td>{{ $post->article }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection