@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">News</h3>

            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <td>Title</td>
                            <td>Cover</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $post)
                        <tr>
                            <td><a href="{{ route('admin.news.show', $post->id) }}">{{ $post->title }}</a></td>
                            <td>
                                @if(isset($post->cover))
                                    <img src="{{ asset("storage/".$post->cover) }}" alt="" class="img-responsive">
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection