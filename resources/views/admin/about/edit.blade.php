@extends('layouts.admin.app')

@section('content')
    <section class="content">
        <div class="box">
            <form action="{{ route('admin.abouts.update', $about->id) }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    <input type="hidden" name="_method" value="put">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="title" value="{!! $about->title ?: old('title') !!}">
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea name="content" id="content" cols="30" rows="10" class="form-control ckeditor" {!! $about->content ?: old('content') !!}></textarea>
                    </div>
                    <div class="form-group">
                        @if(isset($about->cover))
                            <img src="{{ asset("storage/".$about->cover) }}" alt="" class="img-responsive"> <br>
                            <a onclick="return confirm('Are you sure?')" href="#" class="btn btn-danger">Remove Image</a>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="cover">Cover</label>
                        <input type="file" name="cover" id="cover" class="form-control">
                    </div>
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.abouts.index') }}" class="btn btn-default">Back</a>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection