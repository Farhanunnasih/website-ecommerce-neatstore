@extends('layouts.admin.app')

@section('content')
    <section class="section">

        @include('layouts.errors-and-messages')

        <div class="box">
            <form action="{{ route('admin.abouts.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title">Title <span class="text-danger">*</span></label>
                        <input type="text" name="title" id="title" placeholder="title" class="form-control" value="{{ old('title') }}">
                    </div>
                    <div class="form-group">
                        <label for="content">Content <span class="text-danger">*</span></label>
                        <textarea name="content" id="content" rows="5" class="form-control rep-ckeditor" value="{{ old('content') }}"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="cover">Cover</label>
                        <input type="file" name="cover" id="cover" class="form-control">
                    </div>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.abouts.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection