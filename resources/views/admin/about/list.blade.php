@extends('layouts.admin.app')

@section('content')
    <section class="content">
     @include('layouts.errors-and-messages')
    @if($abouts)
        <div class="box">
            <div class="box-body">
                <h2>Abouts</h2>
                <table class="table">
                    <thead>
                        <tr>
                            <td class="col-md-3">Title</td>
                            <td class="col-md-3">Cover</td>
                            <td class="col-md-3">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($abouts as $about)
                        <tr>
                            <td><a href="{{ route('admin.abouts.show', $about->id) }}">{{ $about->title }}</a></td>
                            <td>
                                @if(isset($about->cover))
                                    <img src="{{ asset("storage/".$about->cover) }}" alt="" class="img-responsive">
                                @endif
                            </td>
                            <td>
                                <form action="{{ route('admin.abouts.destroy', $about->id) }}" method="post" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.abouts.edit', $about->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
    </section>
@endsection