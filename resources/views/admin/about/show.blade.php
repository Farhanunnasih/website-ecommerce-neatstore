@extends('layouts.admin.app')

@section('content')
    <section class="content">

        @include('layouts.errors-and-messages')
        @if($about)
            <div class="box">
                <div class="box-body">
                    <h2>About</h2>
                    <table>
                        <thead>
                        <tr>
                            <td class="col-md-4">Title</td>
                            <td class="col-md-4">Cover</td>
                            <td class="col-md-4">Content</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $about->title }}</td>
                            <td>{{ $about->content }}</td>
                            <td>
                                @if(isset($about->cover))
                                    <img src="{{ asset("storage/".$about->cover) }}" alt="" class="img-responsive">
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.abouts.index') }}" class="btn btn-default btn-sm">Back</a>
                    </div>
                </div>
            </div>
        @endif
    </section>
@endsection